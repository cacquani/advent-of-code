class Captcha
  def self.resolve(captcha)
    (captcha + captcha[0])
      .chars
      .chunk { |c| c }
      .map { |c| c[0].to_i * (c[1].length - 1) }
      .reduce(&:+)
  end
end
