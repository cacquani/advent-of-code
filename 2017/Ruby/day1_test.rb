require 'minitest/autorun'
require 'minitest/benchmark'
require './day1'


describe Captcha do
  describe 'resolve the captcha' do
    File.open('../fixtures/01.txt') do |fixtures|
      fixtures.each_line do |line|
        string, expected = line.split(',')
        it "should resolve for #{string}" do
          assert_equal expected.to_i, Captcha.resolve(string)
        end
      end
    end
  end
end

class CaptchaBenchmark < Minitest::Benchmark
  describe 'Captcha Benchmark' do
    File.open('../fixtures/01.txt') do |fixtures|
      fixtures.each_line do |line|
        string, _expected = line.split(',')
        bench_performance_linear string, 0.000001 do |n|
          100.times do
            Captcha.resolve(string)
          end
        end
      end
    end
  end
end
